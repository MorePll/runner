﻿using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    public List<GameObject> enemies;
    public GameObject instantiatePos;
    public float respawningTimer;
    private float time = 0;

    void Start()
    {
        Controller_Enemy.enemyVelocity = 2;
    }

    void Update()
    {
        SpawnEnemies();
        ChangeVelocity();
    }

    private void ChangeVelocity()
    {
        time += Time.deltaTime;
        Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 15f, time / 45f); //cambia la velicidad del enememigo segun el timepo en partida
    }

    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime; //resta time.deltatime a respawntimer
    

        if (respawningTimer <= 0) //si el tiempo de espera es igual o menor a 0 continua el código 
        {
            Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Count)], instantiatePos.transform);
            /*
             Crea un enemigo, usando Instantiate de Unity para crear un nuevo GameObject. 
             el objeto es seleccionado de la lista enemies usando Random.Range 
            */
            respawningTimer = UnityEngine.Random.Range(2, 6); //nuebo tiempo de espera aleatorio
        }
    }
}
