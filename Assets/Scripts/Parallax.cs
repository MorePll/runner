﻿using UnityEngine;

public class Parallax : MonoBehaviour
{
    public GameObject cam;
    private float length, startPos;
    public float parallaxEffect;

    void Start()
    {
        startPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    void Update()
    {
        transform.position = new Vector3(transform.position.x - (parallaxEffect/4), transform.position.y, transform.position.z); //Divido la velocidad del paralax entre 4 para que no sea necesario cambiarlo 1 a 1 desde unity
        if (transform.localPosition.x < -20) // cuando el praralax es x mayor a -20 de mueve a la pocion 20 dando la illucion de que es infinito
        {
            transform.localPosition = new Vector3(20, transform.localPosition.y, transform.localPosition.z); 
        }
        if (Controller_Hud.gameOver) //si la variable gameOver de el sript Controller_Hud es =true la variable parallaxEffect = 0 haciendo que no se mueva
        {
            parallaxEffect = 0;
        }
    }
}
