﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public static bool gameOver = false;
    public Text distanceText;
    public Text gameOverText;
    private float distance = 0;

    void Start()
    {
        gameOver = false;
        distance = 0;
        distanceText.text = distance.ToString();
        gameOverText.gameObject.SetActive(false);
    }

    void Update()
    {
        if (gameOver) //cuando sale true se muestra el hud de game over y se para el tiempo
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over \n Total Distance: " + Mathf.RoundToInt(distance).ToString();
            gameOverText.gameObject.SetActive(true);
        }
        else //se calcula la distancia y se actualiza
        {
            distance += Time.deltaTime;
            distanceText.text = distance.ToString("f0");
        }
    }
}
