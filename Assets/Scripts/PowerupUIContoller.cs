using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerupUIContoller : MonoBehaviour
{
    public Text powerupText;//ref al texto en el canvas
    public InvisibilityPowerup invisibilityPowerup; //Ref al script InvisibilityPowerup

    void Update()
    {
        // Actualizar el texto seg�n el estado del power-up
        powerupText.text = GetPowerupStatus();
    }

    string GetPowerupStatus()
    {
        if (invisibilityPowerup.isInvisible)
        {
            // Si est� invisible, muestra el tiempo restante de invisibilidad
            return "En uso: " + Mathf.CeilToInt(invisibilityPowerup.invisibilityTimer) + "s para terminar";
        }
        if (invisibilityPowerup.cooldownTimer > 0f)
        {
            // Si no est� invisible y el cooldown es mayor a 0, muestra el tiempo restante para el pr�ximo power-up
            return "Sin Power up: " + Mathf.CeilToInt(invisibilityPowerup.cooldownTimer) + "s para el pr�ximo";
        }
        if (invisibilityPowerup.cooldownTimer == 0f)
        {
            // Si el cooldown es igual a 0, muestra "Power up listo"
            return "Power up listo";
        }
        return "Error!";
    }
}
