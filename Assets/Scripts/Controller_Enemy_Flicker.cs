using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Enemy_Flicker : MonoBehaviour
{
    public float flickerSpeed = 0.1f; //velocidad de parpadeo

    private MeshRenderer meshRenderer;
    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>(); //obtine el mesh render del objeto y lo guarda en una variable 

    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;//actualiza el temporizador
        if (timer >= flickerSpeed / 2) //si el temporizador supera la mitad del tiempo de parpadeo
        {
            meshRenderer.enabled = !meshRenderer.enabled;// alternar la visibilidad del mesh renderer
            timer = 0; // Reiniciar el temporizador
        }

    }
}
