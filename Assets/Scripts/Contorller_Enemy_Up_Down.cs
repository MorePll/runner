using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Contorller_Enemy_Up_Down : MonoBehaviour
{
    public float speed = 2f;
    private bool movingUp = true; //controla la direcci�n del movimiento

    void Update()
    {
        
        if (movingUp)
        {
            transform.Translate(Vector3.up * speed * Time.deltaTime); //si da true mueve hacia arriba
        }
        else
        {
            transform.Translate(Vector3.down * speed * Time.deltaTime); //si da false mueve hacia abajo
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor") || collision.gameObject.CompareTag("Roof"))
        {
            movingUp = !movingUp; //cambia entre true y false en cada collision si el tag de objeto es "Floor"
        }
    }
}
