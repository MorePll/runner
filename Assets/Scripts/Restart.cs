﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    void Update()
    {       
        GetInput(); //llama a la funcion
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.R)) //detecta cuando R es precionada
        {
            Time.timeScale = 1; //Hace que el tiempo sea correcto en caso de que este en 0
            SceneManager.LoadScene(0); //recarga la ecena
        }
    }
}
 