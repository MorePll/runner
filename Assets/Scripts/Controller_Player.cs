﻿using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    public InvisibilityPowerup invisible;

    private Rigidbody rb;
    public float jumpForce = 10;
    private float initialSize;
    private int i = 0;
    private bool floored;
    private int counter = 0;
    
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;
    }

    void Update()
    {
        GetInput();
        if (invisible.isInvisible == true)
        {
            floored = false;
        }
    }

    private void GetInput()
    {
        Jump();
        Duck();
    }

    private void Jump()
    {
        if (floored || counter<1) //si esta en el piso o el contador vale menos de 1 = true
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                    rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
                    counter++;
            }
            
        }
        if (counter <= 1 && floored == true) //si el contaqdor es = y > 1 y esta en piso el contador vuelve a 0
        {
            counter = 0;
        }
        else if(invisible.isInvisible == true) //si esta en invisible no hay colision por lo cual se dusca que el posicion en y igual o menor a la posiicon minima del codigo InvisibilityPowerup mas un margen extra 
        {
            if(counter <= 1 && transform.position.y <= (InvisibilityPowerup.minYPosition + 0.4f))
            {
                counter = 0;
            }
        }
    }

    private void Duck()
    {
        if (floored) 
        {
            if (Input.GetKey(KeyCode.S)) 
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);  //divide el tamaño /2 para dar la ilucuion que se agacha
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S)) //si no esta en el piso la fuerza de salto es negativa, osea baja
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
       
        
        if (collision.gameObject.CompareTag("Enemy")) //si colisiona con objeto con tag Enemy, se destruye y se despliega el hud de game over
        {
            Destroy(this.gameObject);
            Controller_Hud.gameOver = true;
        }
        

        if (collision.gameObject.CompareTag("Floor")) //si colosiona con tag Floor esta en el piso 
        {
            floored = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor")) //Si sale de la colisión del objeto con tag floor no está en el piso 
        {
            floored = false;
        }
    }
}
