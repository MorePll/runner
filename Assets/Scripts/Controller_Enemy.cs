﻿using UnityEngine;

public class Controller_Enemy : MonoBehaviour
{
    public static float enemyVelocity;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        rb.AddForce(new Vector3(-enemyVelocity, 0, 0), ForceMode.Force); //moviemiento del enemigo
        OutOfBounds();
    }

    public void OutOfBounds()
    {
        if (this.transform.position.x <= -15) //si la pocision es mayor o igual a -15 se destruye
        {
            Destroy(this.gameObject);
        }
    }
}
