using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisibilityPowerup : MonoBehaviour
{
    public float cooldownDuration = 60f; //duraci�n del cooldown en segundos
    public float invisibilityDuration = 5f; //duraci�n de la invisibilidad en segundos
    public Material invisibleMaterial; // Material de invisibilidad

    private Collider objectCollider; //Collider del objeto
    private Renderer objectRenderer; // Referencia al Renderer del objeto
    private Material originalMaterial; // Mat original
    public float cooldownTimer; //temporizador para el cooldown
    public float invisibilityTimer; // Temporizador para la invisibilidad
    public bool isInvisible = false; // bool para saber si el objeto est� invisible

    public const float minYPosition = 0.00000001234902f; // L�mite m�nimo en el eje Y
    public const float maxYPosition = 8.6f; // L�mite m�ximo en el eje Y
    //esto es porque al desactivar el collider caer�a al vac�o, si no fuese por program�ticamente poner l�mites

    void Start()
    {
        // tener los componentes necesarios del objeto
        objectCollider = GetComponent<Collider>();
        objectRenderer = GetComponent<Renderer>();
        originalMaterial = objectRenderer.material;
        invisibilityTimer = invisibilityDuration;
        cooldownTimer = 0;
    }

    void Update()
    {
        //si presiono la barra espaciadora
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!isInvisible && cooldownTimer <= 0f)
            {
                ActivateInvisibility();
            }
        }

        if (invisibilityTimer > 0f) //si la incisivilidade duro menos del tiempo minimo este sigue bajando
        {
            invisibilityTimer -= Time.deltaTime;
            if (invisibilityTimer <= 0f)//cuando llega a 0
            {
                BackToNormal();
            }
        }

        
        if (cooldownTimer > 0f)
        {
            cooldownTimer -= Time.deltaTime; // Contador para el cooldown funcina muentas dea mayor a 0
            if (cooldownTimer <= 0f)
            {
                cooldownTimer = 0;//si llega a 0 se queda ahi
            }
        }

        if (isInvisible) 
        {
            transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, minYPosition, (maxYPosition - 0.02f)), transform.position.z); //cuando el colider esta inactivo mantiene el objeto entre los limites
        }
    }

    void BackToNormal() 
    {
        //Restaura el objeto (material y collider)
        objectRenderer.material = originalMaterial;
        objectCollider.enabled = true;
        isInvisible = false; //bool a false
        cooldownTimer = cooldownDuration; //reinicia el cooldown
    }
    
    void ActivateInvisibility()
    {
        // cambia el material del objeto al material de invisibilidad
        objectRenderer.material = invisibleMaterial;

        //desactiva el collider del objeto
        objectCollider.enabled = false;

        //empieza el temporizador de invisibilidad 
        invisibilityTimer = invisibilityDuration;

        isInvisible = true; //bool a true

    }
}